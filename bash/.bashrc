#
# ~/.bashrc
#

#alias to display drivers in use 
alias drivers='lspci -v'
#alia for watch the cpu freq
alias cpufreq='watch grep \"cpu MHz\" /proc/cpuinfo'
#alias to see temps via terminal
alias cputemps='watch sensors'
#alias to check disk space
alias diskspace='watch df -h'
#alias for the size of the files 
alias size='ncdu '                      
#alias to see the bios version and other
alias bios='sudo dmidecode | less'
#alias to copy the path of a file to clipboard
alias copyfilepath='read in; readlink -f $in |xclip -sel clip'
#alias for the pwd copy path
alias pwdc='pwd | xclip -selection clipboard'
#alias to anycommand to copy out put 
alias copy='xclip -selection clipboard'
#alias to lsd command
alias la='lsd -lha'
#alias to go to onedrive personal
alias onedrivepersonal='cd ~/OneDrivePersonal'
#alias to mount onedrive
alias monedrive='rclone --vfs-cache-mode writes mount OneDrivePersonal: ~/OneDrivePersonal &'
#alias for the camera using mpv
alias camera='mpv av://v4l2:/dev/video0 --profile=low-latency --untimed'
#alias to clean the orphan package
alias cleanpg='sudo pacman -Rs $(pacman -Qtdq)'
#this is an alias to go to the scripts direcrory
alias scripts='cd ~/.config/scripts'

#This is a alias to go the git repo
alias gitrepo='cd ~/gitrepo'

#set the starship prompt
eval "$(starship init bash)"


echo "hello musicfun"
date
echo "-------------------------------------------------------------------------"
neofetch
echo "-------------------------------------------------------------------------"
