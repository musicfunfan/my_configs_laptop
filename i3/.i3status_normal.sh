#!/bin/sh
# shell script to prepend i3status with more stuff


i3status | while :
do
        #light=$(xbacklight -get); light=${light::-7}
       	updates=$(checkupdates|wc -l)
       	key=$(~/.config/scripts/show_layout.sh)
        uptime=$(uptime -p)
        kernel=$(uname -r)
        gputemp=$(sensors |grep edge | awk '{print $2}')
        hddspace=$(df -h | grep hdd |  awk '{print $5}')
        vms=$(df -h | df -h | grep images | awk '{print $5}')
        snapshot=$(df -h | grep timeshift | awk '{print $5}')
        #this for gpu usage
        radeontop -d gpu.txt -l 1 > /dev/null
        gpuusage=$(cat gpu.txt |awk '{print$5}')
        truncate -s 0 gpu.txt;
	onedrive=$(df -h | grep OneDrivePersonal | awk '{print $5}')
	temp=$(sensors |grep Tccd1 | awk '{print$2}')
        mem=$(awk '/MemTotal/ {memtotal=$2}; /MemAvailable/ {memavail=$2}; END { printf("%.0f", memavail / memtotal * 100) }' /proc/meminfo);
	NowPlaying=$(playerctl metadata | grep title |  playerctl metadata | grep -oP '(?<=title ).*' | sed -e 's/^[ \t]*//')
        read line
       	echo "snapshots-> $snapshot | vms-> $vms | hdd-> $hddspace | onedrive-> $onedrive | ram-> $mem %| gpu-> usage-> $gpuusage |gpu-> $gputemp | cpu-> $temp | $line | $updates 📦 | $key | ❤ $kernel | $uptime" || exit 1
done
