# Dependencies fot the i3wm config file

* i3wm 
* alacritty 
* feh
* power.sh (script)
* sites.sh (script)
* rofi
* backup.sh (script)
* media.sh (script)
* firefox
* pcmanfm
* pavucontrol
* screenshot.sh (script)
* volumeMeter.sh (script)
* playerctl 
* betterlockscreen
* language.sh (script)
* picom 
* lxpolkit
* gnome-keyring 
* rclone
* dunst
* redshift
* radeontop 
* polybar 
* openrgb 
* skypeforlinux
* thunderbird
* discord 
* element 
* pipewire
 
# Explanation 

All the above apps and scripts is not hard dependencies is just what i use with i3wm. If you do not have something from all the above is all good, you just would not have the features that the above scripts and programs add in my system.